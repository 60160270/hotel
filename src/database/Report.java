/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Administrator
 */
public class Report {

    int room_id;
    int stay_id;
    String checkindatetime;
    String checkoutdatetime;
    double total_price;
    
    public Report(int stay_id,  int room_id,  String checkindatetime, String checkoutdatetime,double total_price) {
        this.room_id = stay_id;
        this.stay_id = room_id;
        this.checkindatetime = checkindatetime;
        this.checkoutdatetime = checkoutdatetime;
        this.total_price = total_price;
    }

    public int getStay_id() {
        return room_id;
    }

    public void setStay_id(int room_id) {
        this.room_id = room_id;
    }

    public int getRoom_id() {
        return stay_id;
    }

    public void setRoom_id(int stay_id) {
        this.stay_id = stay_id;
    }

    public String getCheckindatetime() {
        return checkindatetime;
    }

    public void setCheckindatetime(String checkindatetime) {
        this.checkindatetime = checkindatetime;
    }

    public String getCheckoutdatetime() {
        return checkoutdatetime;
    }

    public void setCheckoutdatetime(String checkoutdatetime) {
        this.checkoutdatetime = checkoutdatetime;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }
    
}
