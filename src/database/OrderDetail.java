/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Thanakrit Wutthiphithak 60160247
 */
public class OrderDetail {

    int dorder_id;
    int dorder_amount;
    double dorder_price;
    int product_id;
    int order_id;

    public OrderDetail() {
    }

    public OrderDetail(int dorder_id, int dorder_amount, double dorder_price, int product_id, int order_id) {
        this.dorder_id = dorder_id;
        this.dorder_amount = dorder_amount;
        this.dorder_price = dorder_price;
        this.product_id = product_id;
        this.order_id = order_id;
    }

    public int getDorder_id() {
        return dorder_id;
    }

    public void setDorder_id(int dorder_id) {
        this.dorder_id = dorder_id;
    }

    public int getDorder_amount() {
        return dorder_amount;
    }

    public void setDorder_amount(int dorder_amount) {
        this.dorder_amount = dorder_amount;
    }

    public double getDorder_price() {
        return dorder_price;
    }

    public void setDorder_price(double dorder_price) {
        this.dorder_price = dorder_price;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "dorder_id=" + dorder_id + ", dorder_amount=" + dorder_amount + ", dorder_price=" + dorder_price + ", product_id=" + product_id + ", order_id=" + order_id + '}';
    }

}
