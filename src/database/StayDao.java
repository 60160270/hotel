/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Supawee
 */
public class StayDao {

    public static boolean insert(Stay stay) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "INSERT INTO stay (\n"
                    + "                     staff_id,\n"
                    + "                     room_id,\n"
                    + "                     stay_checkoutdatetime,\n"
                    + "                     stay_checkindatetime,\n"
                    + "                     stay_status,\n"
                    + "                     stay_amount,\n"
                    + "                     stay_tell,\n"
                    + "                     stay_age,\n"
                    + "                     stay_surname,\n"
                    + "                     stay_name,\n"
                    + "                     stay_idcard,\n"
                    + "                     stay_id\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     '%d',\n"
                    + "                     '%d',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%d',\n"
                    + "                     '%s',\n"
                    + "                     '%d',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%d'\n"
                    + "                 );";
            stm.execute(String.format(sql, stay.getStaffid(), stay.getRoomid(), stay.getCheckindatetime(), stay.getCheckoutdatetime(), stay.getStatus(), stay.getAmount(), stay.getTell(), stay.getAge(), stay.getSurname(), stay.getName(), stay.getIdcard(), stay.getStayid()));
            System.out.println("insert success");
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StayDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean update(Stay stay) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "UPDATE stay\n"
                    + "   SET stay_tell = '%s',\n"
                    + "       stay_amount = %d,\n"
                    + "       stay_status = '%s',\n"
                    + "       stay_checkindatetime = '%s',\n"
                    + "       stay_checkoutdatetime = '%s',\n"
                    + "       room_id = %d,\n"
                    + "       staff_id = %d\n"
                    + " WHERE stay_id = %d;";

            stm.execute(String.format(sql, stay.getTell(), stay.getAmount(), stay.getStatus(), stay.getCheckindatetime(), stay.getCheckoutdatetime(), stay.getRoomid(), stay.getStaffid(), stay.getStayid()));
            System.out.println("update success");
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StayDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean delete(Stay stay) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "DELETE FROM stay\n"
                    + "      WHERE stay_id = '%d';";
            stm.execute(String.format(sql, stay.getStayid()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StayDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static ArrayList<Stay> getStays() {
        ArrayList<Stay> List = new ArrayList<>();
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT stay_id,\n"
                    + "       stay_idcard,\n"
                    + "       stay_name,\n"
                    + "       stay_surname,\n"
                    + "       stay_age,\n"
                    + "       stay_tell,\n"
                    + "       stay_amount,\n"
                    + "       stay_status,\n"
                    + "       stay_checkindatetime,\n"
                    + "       stay_checkoutdatetime,\n"
                    + "       room_id,\n"
                    + "       staff_id\n"
                    + "  FROM stay";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Stay stay = toObject(rs);
                List.add(stay);
            }
            Database.closedb();
            return List;
        } catch (SQLException ex) {
            Logger.getLogger(StayDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    public static Stay getStay(int id) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT * FROM stay\n"
                    + " WHERE stay_id = '%d';";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            if (rs.next()) {
                Stay stay = toObject(rs);
                Database.closedb();
                return stay;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StayDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    public static ArrayList<Report> getReport(String Month, String Year) {
        ArrayList<Report> List = new ArrayList<Report>();
        String sql = "select room.room_price as roomPrice,stay_id,room.room_id,stay_checkindatetime,stay_checkoutdatetime,\n"
                + " ((strftime('%d',stay_checkoutdatetime)-strftime('%d',stay_checkindatetime))*room.room_price) as total_price \n"
                + "from stay ,room\n"
                + "where \n"
                + "strftime('%m',stay_checkindatetime) = \"" + Month + "\" AND\n"
                + "strftime('%Y',stay_checkindatetime) = \"" + Year + "\"  AND\n"
                + "stay.stay_checkoutdatetime is not null  and\n"
                + "room.room_id = stay.room_id\n"
                + "order by stay_checkoutdatetime DESC;";
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Report report = toObjectReport(rs);
                List.add(report);
            }
            Database.closedb();
            return List;

        } catch (SQLException ex) {
            Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    private static Stay toObject(ResultSet rs) throws SQLException {
        Stay stay;
        stay = new Stay();
        stay.setStayid(rs.getInt("stay_id"));
        stay.setIdcard(rs.getString("stay_idcard"));
        stay.setName(rs.getString("stay_name"));
        stay.setSurname(rs.getString("stay_surname"));
        stay.setAge(rs.getInt("stay_age"));
        stay.setTell(rs.getString("stay_tell"));
        stay.setAmount(rs.getInt("stay_amount"));
        stay.setStatus(rs.getString("stay_status"));
        stay.setCheckindatetime(rs.getString("stay_checkindatetime"));
        stay.setCheckoutdatetime(rs.getString("stay_checkoutdatetime"));
        stay.setRoomid(rs.getInt("room_id"));
        stay.setStaffid(rs.getInt("staff_id"));
        return stay;
    }

    private static Report toObjectReport(ResultSet rs) throws SQLException {
        Report report;
        double total_price = rs.getDouble("total_price");
        total_price = (total_price == 0) ? rs.getDouble("roomPrice") : total_price;
        report = new Report(
                rs.getInt("stay_id"),
                rs.getInt("room_id"),
                rs.getString("stay_checkindatetime"),
                rs.getString("stay_checkoutdatetime"),
                total_price
        );
        return report;
    }

}
