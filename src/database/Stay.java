/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Supawee
 */
public class Stay {

    public int getStayid() {
        return stayid;
    }

    public void setStayid(int stayid) {
        this.stayid = stayid;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTell() {
        return tell;
    }

    public void setTell(String tell) {
        this.tell = tell;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCheckindatetime() {
        return checkindatetime;
    }

    public void setCheckindatetime(String checkindatetime) {
        this.checkindatetime = checkindatetime;
    }

    public String getCheckoutdatetime() {
        return checkoutdatetime;
    }

    public void setCheckoutdatetime(String checkoutdatetime) {
        this.checkoutdatetime = checkoutdatetime;
    }

    public int getRoomid() {
        return roomid;
    }

    public void setRoomid(int roomid) {
        this.roomid = roomid;
    }

    public int getStaffid() {
        return staffid;
    }

    public void setStaffid(int staffid) {
        this.staffid = staffid;
    }

    public Stay(String idcard, String name, String surname, int age, String tell, int amount, String status, String checkindatetime, String checkoutdatetime, int roomid, int staffid) {
        this.idcard = idcard;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.tell = tell;
        this.amount = amount;
        this.status = status;
        this.checkindatetime = checkindatetime;
        this.checkoutdatetime = checkoutdatetime;
        this.roomid = roomid;
        this.staffid = staffid;
    }
    int stayid;
    String idcard;
    String name;
    String surname;
    int age;
    String tell;
    int amount;
    String status;
    String checkindatetime;
    String checkoutdatetime;
    int roomid;
    int staffid;

    public Stay() {
    }

}
