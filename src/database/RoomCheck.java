/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import javax.swing.JOptionPane;
import org.omg.CORBA.INTERNAL;

/**
 *
 * @author Administrator
 */
public class RoomCheck {

    public static boolean CheckRoomId(Object id) {
        try {
            Integer.parseInt((String) id);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Please input ID ROOM information in numbers.");
            return false;
        }
    }

    public static boolean CheckPrice(Object id) {
        try {
            Double.parseDouble((String) id);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Please input Price information in numbers.");
            return false;
        }
    }

    public static boolean CheckType(Object id) {
        if (id.equals("2P") || id.equals("4P")) {
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Please Input Type is 2P Or 4P.");
            return false;
        }
    }
    
}
