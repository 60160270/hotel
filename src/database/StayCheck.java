/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrator
 */
public class StayCheck {

    public static boolean CheckIdCard(Object id) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT * FROM staff WHERE staff_idcard=%s";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            Database.closedb();
            if (rs.next()) {
                JOptionPane.showMessageDialog(null, "The ID CARD you entered already exists.");
                return false;
            } else {
                return true;
            }

        } catch (SQLException ex) {
            return true;
        }

    }

    public static boolean CheckName(Object id) {
        try {
            Integer.parseInt((String) id);
            JOptionPane.showMessageDialog(null, "Please input the name in the correct field.");
            return false;
        } catch (Exception e) {
            return true;
        }

    }

    public static boolean CheckSurname(Object id) {
        try {
            Integer.parseInt((String) id);
            JOptionPane.showMessageDialog(null, "Please input the Surname in the correct field.");
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public static boolean CheckTell(Object id) {
        try {
            Integer.parseInt((String) id);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Please input Tell information in numbers.");
            return false;
        }
    }

    public static boolean CheckAge(Object id) {
        try {
            Integer.parseInt((String) id);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Please input Age information in numbers.");
            return false;
        }
    }

    public static boolean CheckAmount(Object id) {
        try {
            Integer.parseInt((String) id);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Please input information in numbers.");
            return false;
        }
    }

    public static boolean CheckStaffID(Object id) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT * FROM staff WHERE staff_id=%s";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            Database.closedb();
            if (rs.next()) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "The Staff ID you entered is not available in the database.");
                return false;
            }

        } catch (SQLException ex) {
            return true;
        }
    }

    public static boolean CheckRoomID(Object id) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT * FROM staff WHERE room_id=%s";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            Database.closedb();
            if (rs.next()) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "The Room ID you entered is not available in the database.");
                return false;
            }

        } catch (SQLException ex) {
            return true;
        }
    }

}
