/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import javax.swing.JOptionPane;

/**
 *
 * @author Administrator
 */
public class ProductCheck {

    public static boolean CheckName(Object id) {
        if (id != null) {
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Please input a Product Name");
            return false;
        }

    }

    public static boolean CheckPrice(Object id) {
        try {
            Double.parseDouble((String) id);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Please input information in numbers.");
            return false;
        }
    }

    public static boolean CheckAmount(Object id) {
        try {
            Integer.parseInt((String) id);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Please input information in numbers.");
            return false;
        }
    }
}
