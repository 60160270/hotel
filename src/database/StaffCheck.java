/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrator
 */
public class StaffCheck {

    public static boolean CheckIdCard(Object id) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT * FROM staff WHERE staff_idcard=%s";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            Database.closedb();
            if (rs.next()) {
                JOptionPane.showMessageDialog(null, "The ID CARD you entered already exists.");
                return false;
            } else {
                return true;
            }

        } catch (SQLException ex) {
            return true;
        }

    }

    public static boolean CheckName(Object id) {
        try {
            Integer.parseInt((String) id);
            JOptionPane.showMessageDialog(null, "Please input the name in the correct field.");
            return false;
        } catch (Exception e) {
            return true;
        }

    }

    public static boolean CheckSurname(Object id) {
        try {
            Integer.parseInt((String) id);
            JOptionPane.showMessageDialog(null, "Please input the Surname in the correct field.");
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public static boolean CheckTell(Object id) {
        try {
            Integer.parseInt((String) id);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Please input Tell information in numbers.");
            return false;
        }
    }

    public static boolean CheckBirthday(String id) {
        if (id.length() == 10) {
            if (id.charAt(4) == '-' && id.charAt(7) == '-') {
                return true;
            }
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Please input your birthday correctly.");
            return false;
        }
    }

    public static boolean CheckUsername(int id) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT * FROM staff WHERE staff_username=%s";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            Database.closedb();
            if (rs.next()) {
                JOptionPane.showMessageDialog(null, "The username you entered already exists.");
                return false;
            } else {
                return true;
            }

        } catch (SQLException ex) {
            return true;
        }

    }

}
