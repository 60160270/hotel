/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author informatics
 */
public class Order {

    public Order(int order_id, String order_datetime, int order_amount, double order_totalprice, int staff_id, int stay_id) {
        this.order_id = order_id;
        this.order_datetime = order_datetime;
        this.order_amount = order_amount;
        this.order_totalprice = order_totalprice;
        this.staff_id = staff_id;
        this.stay_id = stay_id;
    }

    Order() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public String getOrder_datetime() {
        return order_datetime;
    }

    public void setOrder_datetime(String order_datetime) {
        this.order_datetime = order_datetime;
    }

    public int getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(int order_amount) {
        this.order_amount = order_amount;
    }

    public double getOrder_totalprice() {
        return order_totalprice;
    }

    public void setOrder_totalprice(double order_totalprice) {
        this.order_totalprice = order_totalprice;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public int getStay_id() {
        return stay_id;
    }

    public void setStay_id(int stay_id) {
        this.stay_id = stay_id;
    }
    int order_id;
    String order_datetime;
    int order_amount;
    double order_totalprice;
    int staff_id;
    int stay_id;
}
