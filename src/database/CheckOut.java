/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Administrator
 */
public class CheckOut {

    String Product_name;
    int amount;
    double price;

    public String getProduct_name() {
        return Product_name;
    }

    public void setRoom_id(int room_id) {
        this.Product_name = Product_name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public CheckOut(String Product_name,  int amount,  double price) {
        this.Product_name = Product_name;
        this.amount = amount;
        this.price = price;
        
    }

    
    
}
