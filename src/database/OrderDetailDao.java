/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thanakrit Wutthiphithak 60160247
 */
public class OrderDetailDao {

    public static boolean insert(OrderDetail orderDetail) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO detail_order (\n"
                    + "                             order_id,\n"
                    + "                             product_id,\n"
                    + "                             dorder_price,\n"
                    + "                             dorder_amount\n"
                    + "                         )\n"
                    + "                         VALUES (\n"
                    + "                             %d,\n"
                    + "                             %d,\n"
                    + "                             %f,\n"
                    + "                             %d\n"
                    + "                         );";
            stm.execute(String.format(sql, orderDetail.getOrder_id(), orderDetail.getProduct_id(), orderDetail.getDorder_price(), orderDetail.getDorder_amount()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean update(OrderDetail orderDetail) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE detail_order\n"
                    + "   SET dorder_amount = %d\n"
                    + " WHERE dorder_id = %d;";
            stm.execute(String.format(sql, orderDetail.getDorder_amount(), orderDetail.getDorder_id()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean delete(int dorder_id) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "DELETE FROM detail_order\n"
                    + "      WHERE dorder_id = %d;";
            stm.execute(String.format(sql, dorder_id));
            Database.connectdb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static ArrayList<OrderDetail> getOrderProducts(int order_id) {
        ArrayList<OrderDetail> orderDetailList = new ArrayList<>();
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT dorder_id,\n"
                    + "       dorder_amount,\n"
                    + "       dorder_price,\n"
                    + "       product_id,\n"
                    + "       order_id\n"
                    + "  FROM detail_order\n"
                    + " WHERE order_id = %d;";
            ResultSet rs = stm.executeQuery(String.format(sql, order_id));
            while (rs.next()) {
                OrderDetail orderDetail = toObject(rs);
                orderDetailList.add(orderDetail);
            }
            Database.closedb();
            return orderDetailList;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    public static OrderDetail getOrderProduct(int order_id, int dorder_id) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT dorder_id,\n"
                    + "       dorder_amount,\n"
                    + "       dorder_price,\n"
                    + "       product_id,\n"
                    + "       order_id\n"
                    + "  FROM detail_order\n"
                    + " WHERE order_id = %d AND \n"
                    + "       dorder_id = %d;";
            ResultSet rs = stm.executeQuery(String.format(sql, order_id, dorder_id));
            if (rs.next()) {
                OrderDetail orderDetail = toObject(rs);
                Database.closedb();
                return orderDetail;
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    private static OrderDetail toObject(ResultSet rs) throws SQLException {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setDorder_id(rs.getInt("dorder_id"));
        orderDetail.setDorder_amount(rs.getInt("dorder_amount"));
        orderDetail.setDorder_price(rs.getDouble("dorder_price"));
        orderDetail.setProduct_id(rs.getInt("product_id"));
        orderDetail.setOrder_id(rs.getInt("order_id"));
        return orderDetail;
    }

}
