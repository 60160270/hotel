/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.nashorn.internal.objects.Global.toObject;

/**
 *
 * @author informatics
 */
public class CheckOutDao {

    public static ArrayList<Order> getOrder(int stayId) {
        ArrayList<Order> OrderList = new ArrayList<>();
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM [order] where stay_id = " + stayId;
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Order order = toObject(rs);
                OrderList.add(order);
            }
            Database.closedb();
            return OrderList;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    public static ArrayList<OrderDetail> getOrderProducts(int order_id) {
        ArrayList<OrderDetail> orderDetailList = new ArrayList<>();
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT dorder_id,\n"
                    + "       dorder_amount,\n"
                    + "       dorder_price,\n"
                    + "       product_id,\n"
                    + "       order_id\n"
                    + "  FROM detail_order\n"
                    + " WHERE order_id = %d;";
            ResultSet rs = stm.executeQuery(String.format(sql, order_id));
            while (rs.next()) {
                OrderDetail orderDetail = toObjectdetail(rs);
                orderDetailList.add(orderDetail);
            }
            Database.closedb();
            return orderDetailList;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    private static Order toObject(ResultSet rs) throws SQLException {
        Order order = new Order(
                rs.getInt("order_id"), rs.getString("order_datetime"), rs.getInt("order_amount"), rs.getDouble("order_totalprice"), rs.getInt("staff_id"), rs.getInt("stay_id")
        );
        
        return order;
    }

    private static OrderDetail toObjectdetail(ResultSet rs) throws SQLException {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setDorder_id(rs.getInt("dorder_id"));
        orderDetail.setDorder_amount(rs.getInt("dorder_amount"));
        orderDetail.setDorder_price(rs.getDouble("dorder_price"));
        orderDetail.setProduct_id(rs.getInt("product_id"));
        orderDetail.setOrder_id(rs.getInt("order_id"));
        return orderDetail;
    }
}
