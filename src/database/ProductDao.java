/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thanakrit Wutthiphithak 60160247
 */
public class ProductDao {

    public static boolean insert(Product product) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO product (\n"
                    + "                        product_name,\n"
                    + "                        product_price,\n"
                    + "                        product_amount\n"
                    + "                        \n"
                    + "                    )\n"
                    + "                    VALUES (\n"
                    + "                        '%s',\n"
                    + "                        %f,\n"
                    + "                        %d\n"
                    + "                    );";
            stm.execute(String.format(sql, product.getProduct_name(), product.getProduct_price(), product.getProduct_amount()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean update(Product product) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE product\n"
                    + "   SET product_name = '%s',\n"
                    + "       product_amount = %d,\n"
                    + "       product_price = %f\n"
                    + " WHERE product_id =  %d;";
            stm.execute(String.format(sql, product.getProduct_name(), product.getProduct_amount(), product.getProduct_price(), product.getProduct_id()));
            Database.closedb();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean delete(Product product) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "DELETE FROM product\n"
                    + "      WHERE product_id = %d;";
            stm.execute(String.format(sql, product.getProduct_id()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static Product getProduct(int product_id) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * "
                    + "  FROM product "
                    + " WHERE product_id = %d";
            ResultSet rs = stm.executeQuery(String.format(sql, product_id));
            if (rs.next()) {
                Product product = toObject(rs);
                Database.closedb();
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    public static ArrayList<Product> getProducts() {
        ArrayList<Product> productList = new ArrayList<>();
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT product_id,\n"
                    + "       product_name,\n"
                    + "       product_amount,\n"
                    + "       product_price\n"
                    + "  FROM product";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Product product = toObject(rs);
                productList.add(product);
            }
            Database.closedb();
            return productList;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    private static Product toObject(ResultSet rs) throws SQLException {
        Product product = new Product();
        product.setProduct_id(rs.getInt("product_id"));
        product.setProduct_name(rs.getString("product_name"));
        product.setProduct_amount(rs.getInt("product_amount"));
        product.setProduct_price(rs.getInt("product_price"));
        return product;
    }

    public static boolean minusAmount(OrderDetail orderDetail) {
        Product product = ProductDao.getProduct(orderDetail.getProduct_id());
        int updateAmount = product.getProduct_amount() - orderDetail.getDorder_amount();
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE product\n"
                    + "   SET product_amount = %d\n"
                    + " WHERE product_id = %d;";
            stm.execute(String.format(sql, updateAmount, orderDetail.getProduct_id()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean plusAmount(OrderDetail orderDetail) {
        Product product = ProductDao.getProduct(orderDetail.getProduct_id());
        int updateAmount = product.getProduct_amount() + orderDetail.getDorder_amount();
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE product\n"
                    + "   SET product_amount = %d\n"
                    + " WHERE product_id = %d;";
            stm.execute(String.format(sql, updateAmount, orderDetail.getProduct_id()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean editAmount(OrderDetail orderDetail, int updateAmount) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE product\n"
                    + "   SET product_amount = %d\n"
                    + " WHERE product_id = %d;";
            stm.execute(String.format(sql, updateAmount, orderDetail.getProduct_id()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

}
