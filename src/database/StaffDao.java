/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class StaffDao {

    public static boolean insert(Staff staff) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "INSERT INTO staff (\n"
                    + "                      staff_type,\n"
                    + "                      staff_password,\n"
                    + "                      staff_username,\n"
                    + "                      staff_tell,\n"
                    + "                      staff_birthday,\n"
                    + "                      staff_surname,\n"
                    + "                      staff_name,\n"
                    + "                      staff_idcard\n"
                    + "                  )\n"
                    + "                  VALUES (\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s'\n"
                    + "                  );";
            stm.execute(String.format(sql, staff.getType(), staff.getPassword(), staff.getUsername(), staff.getTell(), staff.getBirthday(), staff.getSurname(), staff.getName(), staff.getIdcard()));
            System.out.println("insert success");
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean update(Staff staff) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "UPDATE staff\n"
                    + "   SET staff_name = '%s',\n"
                    + "       staff_surname = '%s',\n"
                    + "       staff_tell = '%s',\n"
                    + "       staff_password = '%s',\n"
                    + "       staff_type = '%s'\n"
                    + " WHERE staff_id = '%d';";
            stm.execute(String.format(sql, staff.getName(), staff.getSurname(), staff.getTell(), staff.getPassword(), staff.getType(), staff.getId()));
            System.out.println("update success");
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean delete(Staff staff) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "DELETE FROM staff\n"
                    + "      WHERE staff_id = '%d';";
            stm.execute(String.format(sql, staff.getId()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static ArrayList<Staff> getStaffs() {
        ArrayList<Staff> List = new ArrayList<>();
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT staff_id,\n"
                    + "       staff_idcard,\n"
                    + "       staff_name,\n"
                    + "       staff_surname,\n"
                    + "       staff_birthday,\n"
                    + "       staff_tell,\n"
                    + "       staff_username,\n"
                    + "       staff_password,\n"
                    + "       staff_type\n"
                    + "  FROM staff\n";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Staff staff = toObject(rs);
                List.add(staff);
            }
            Database.closedb();
            return List;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    public static Staff getStaff(int id) {
        Connection connect = Database.connectdb();
        try {
            Statement stm = connect.createStatement();
            String sql = "SELECT * FROM staff WHERE staff_id=%d";
            ResultSet rs = stm.executeQuery(String.format(sql, id));
            if (rs.next()) {
                Staff staff = toObject(rs);
                Database.closedb();
                return staff;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    private static Staff toObject(ResultSet rs) throws SQLException {
        //int id, String idcard, String name, String surname, String birthday, String tell, String username, String password, String type
        Staff staff;
        staff = new Staff();
        staff.setId(rs.getInt("staff_id"));
        staff.setIdcard(rs.getString("staff_idcard"));
        staff.setName(rs.getString("staff_name"));
        staff.setSurname(rs.getString("staff_surname"));
        staff.setBirthday(rs.getString("staff_birthday"));
        staff.setTell(rs.getString("staff_tell"));
        staff.setUsername(rs.getString("staff_username"));
        staff.setPassword(rs.getString("staff_password"));
        staff.setType(rs.getString("staff_type"));
        return staff;
    }

}
