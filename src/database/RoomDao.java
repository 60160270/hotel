/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class RoomDao {

    public static boolean update(Room room) {
        try {
            String sql = "UPDATE room\n"
                    + "   SET room_id = %d,\n"
                    + "       room_type = '%s',\n"
                    + "       room_price = '%s',\n"
                    + "       room_status = '%s'\n"
                    + "   WHERE room_id = %d;";
            System.out.println(String.format(sql, room.getRoom_id(), room.getRoom_type(), room.getRoom_price(), room.getRoom_status(), room.getRoom_id()));
            Connection conn = Database.connectdb();
            Statement stm = conn.createStatement();
            stm.execute(String.format(sql, room.getRoom_id(), room.getRoom_type(), room.getRoom_price(), room.getRoom_status(), room.getRoom_id()));

            Database.closedb();

        } catch (Exception e) {
            System.out.println("aaa");
        }
        return true;
    }

    public static ArrayList<Room> getRoom() {
        ArrayList<Room> roomList = new ArrayList<>();
        Connection conn = Database.connectdb();

        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT room_id,\n"
                    + "       room_type,\n"
                    + "       room_price,\n"
                    + "       room_status\n"
                    + "  FROM room";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Room room = toObject(rs);
                roomList.add(room);
            }
            Database.closedb();
            return roomList;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    public static Room getRoom(int room_id) {
        String sql = "SELECT * FROM room WHERE room_id = %d";
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, room_id));
            if (rs.next()) {
                Room room = toObject(rs);
                Database.closedb();
                return room;
            }

        } catch (SQLException ex) {
            Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    private static Room toObject(ResultSet rs) throws SQLException {
        Room room = new Room();
        room.setRoom_id(rs.getInt("room_id"));
        room.setRoom_type(rs.getString("Room_type"));
        room.setRoom_price(rs.getDouble("Room_price"));
        room.setRoom_status(rs.getString("Room_status"));
        return room;
    }

}
