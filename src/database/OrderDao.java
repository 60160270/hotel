/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.nashorn.internal.objects.Global.toObject;

/**
 *
 * @author informatics
 */
public class OrderDao {

    public static boolean insert(Order order) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO [order] (\n"
                    + "                        stay_id,\n"
                    + "                        staff_id,\n"
                    + "                        order_totalprice,\n"
                    + "                        order_amount,\n"
                    + "                        order_datetime,\n"
                    + "                        order_id\n"
                    + "                    )\n"
                    + "                    VALUES (\n"
                    + "                        %d,\n"
                    + "                        %d,\n"
                    + "                        %f,\n"
                    + "                        %d,\n"
                    + "                        %s,\n"
                    + "                        %d\n"
                    + "                    );";
            stm.execute(String.format(sql, order.getStay_id(), order.getStaff_id(), order.getOrder_totalprice(), order.getOrder_amount(), order.getOrder_datetime(), order.getOrder_id()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean update(int sum_amount, double total, int order_id) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE [order]\n"
                    + "   SET order_amount = %d,\n"
                    + "       order_totalprice = %f\n"
                    + " WHERE order_id = %d;";
            stm.execute(String.format(sql, sum_amount, total, order_id));
            Database.closedb();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static boolean delete(Order order) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "DELETE FROM order\n"
                    + "      WHERE order_id = %d;";
            stm.execute(String.format(sql, order.getOrder_id()));
            Database.closedb();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return false;
    }

    public static Order getOrder(int order_id) {
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * "
                    + "  FROM order "
                    + " WHERE order_id = %d";
            ResultSet rs = stm.executeQuery(String.format(sql, order_id));
            if (rs.next()) {
                Order order = toObject(rs);
                Database.closedb();
                return order;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    public static ArrayList<Order> getOrder() {
        ArrayList<Order> OrderList = new ArrayList<>();
        Connection conn = Database.connectdb();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT order_datetime = '%s',\n"
                    + "       order_amount = %d,\n"
                    + "       order_totalprice = %f,\n"
                    + "       staff_id = %d\n"
                    + "       stay_id = %d\n"
                    + "  FROM order";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Order order = toObject(rs);
                OrderList.add(order);
            }
            Database.closedb();
            return OrderList;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.closedb();
        return null;
    }

    private static Order toObject(ResultSet rs) throws SQLException {
        Order order = new Order();
        order.setOrder_id(rs.getInt("order_id"));
        order.setOrder_datetime(rs.getString("order_datetime"));
        order.setOrder_amount(rs.getInt("order_amount"));
        order.setOrder_totalprice(rs.getDouble("order_totalprice"));
        order.setStaff_id(rs.getInt("staff_id"));
        order.setStay_id(rs.getInt("stay_id"));
        return order;
    }
}
