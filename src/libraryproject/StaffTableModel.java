/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Staff;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Supawee
 */
public class StaffTableModel extends AbstractTableModel {

    ArrayList<Staff> staff_list = new ArrayList<>();
    //String idcard, String name, String surname, String birthday, String tell, String username, String password, String type
    String[] columnNames = {"ID", "ID Card", "Name", "Surname", "Birthday", "Tell", "Username", "Password", "Type"};

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return staff_list.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Staff staff = staff_list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return staff.getId();
            case 1:
                return staff.getIdcard();
            case 2:
                return staff.getName();
            case 3:
                return staff.getSurname();
            case 4:
                return staff.getBirthday();
            case 5:
                return staff.getTell();
            case 6:
                return staff.getUsername();
            case 7:
                return staff.getPassword();
            case 8:
                return staff.getType();
        }
        return null;
    }

    public void setData(ArrayList<Staff> staff_list) {
        this.staff_list = staff_list;
        fireTableDataChanged();
    }

}
