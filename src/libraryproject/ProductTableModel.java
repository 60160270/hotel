/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Product;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Thanakrit Wutthiphithak 60160247
 */
public class ProductTableModel extends AbstractTableModel {

    ArrayList<Product> productList = new ArrayList<>();
    String[] columnNames = {"Product ID", "Product Name", "Price", "Amount"};

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return productList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Product product = productList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return product.getProduct_id();
            case 1:
                return product.getProduct_name();
            case 2:
                return product.getProduct_price();
            case 3:
                return product.getProduct_amount();
        }

        return "";
    }

    public void setData(ArrayList<Product> productList) {
        this.productList = productList;
        fireTableDataChanged();
    }
}
