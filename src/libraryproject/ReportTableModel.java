/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Report;
import database.Stay;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Asus
 */
public class ReportTableModel extends AbstractTableModel {

    ArrayList<Report> reportlist = new ArrayList<Report>();
    String[] columnReport = {"Stay ID", "Room ID", "Check In DateTime", "Check Out DateTime", "Total Price"};

    @Override
    public int getRowCount() {
        return reportlist.size();
    }

    public String getColumnName(int column) {
        return columnReport[column];
    }

    @Override
    public int getColumnCount() {
        return columnReport.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columIndex) {
        Report report = reportlist.get(rowIndex);
        switch (columIndex) {
            case 0:
                return report.getStay_id();
            case 1:
                return report.getRoom_id();
            case 2:
                return report.getCheckindatetime();
            case 3:
                return report.getCheckoutdatetime();
            case 4:
                return report.getTotal_price();

        }
        return "";
    }

    public void setData(ArrayList<Report> reportlist) {
        this.reportlist = reportlist;
        fireTableDataChanged();
    }

}
