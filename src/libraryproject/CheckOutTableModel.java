/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.CheckOut;
import database.Product;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Thanakrit Wutthiphithak 60160247
 */
public class CheckOutTableModel extends AbstractTableModel {

    ArrayList<CheckOut> checkoutList = new ArrayList<>();
    String[] columnNames = {"Product", "Amount", "Price"};

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return checkoutList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        CheckOut checkout = checkoutList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return checkout.getProduct_name();
            case 1:
                return checkout.getAmount();
            case 2:
                return checkout.getPrice();
            
        }

        return "";
    }

    public void setData(ArrayList<CheckOut> checkoutList) {
        this.checkoutList = checkoutList;
        fireTableDataChanged();
    }
}
