/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.OrderDetail;
import database.ProductDao;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Thanakrit Wutthiphithak 60160247
 */
public class OrderTableModel extends AbstractTableModel {

    ArrayList<OrderDetail> orderDetailList = new ArrayList<>();
    String[] columnNames = {"#", "Product Name", "Price", "Unit"};

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return orderDetailList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        OrderDetail orderDetail = orderDetailList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return orderDetail.getDorder_id();
            case 1:
                return ProductDao.getProduct(orderDetail.getProduct_id()).getProduct_name();
            case 2:
                return orderDetail.getDorder_price();
            case 3:
                return orderDetail.getDorder_amount();
        }
        return "";
    }

    void setData(ArrayList<OrderDetail> orderDetailList) {
        this.orderDetailList = orderDetailList;
        fireTableDataChanged();
    }

}
