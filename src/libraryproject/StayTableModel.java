/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Stay;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Supawee
 */
public class StayTableModel extends AbstractTableModel {

    ArrayList<Stay> stay_list = new ArrayList<>();
    //String idcard, String name, String surname, String birthday, String tell, String username, String password, String type
    String[] columnNames = {"ID", "ID Card", "Name", "Surname", "Tell", "Age", "Amount", "Staff ID", "Room ID", "Status", "Check In DateTime", "Check Out DateTime"};

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return stay_list.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Stay stay = stay_list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return stay.getStayid();
            case 1:
                return stay.getIdcard();
            case 2:
                return stay.getName();
            case 3:
                return stay.getSurname();
            case 4:
                return stay.getTell();
            case 5:
                return stay.getAge();
            case 6:
                return stay.getAmount();
            case 7:
                return stay.getStaffid();
            case 8:
                return stay.getRoomid();
            case 9:
                return stay.getStatus();
            case 10:
                return stay.getCheckindatetime();
            case 11:
                return stay.getCheckoutdatetime();
        }
        return null;
    }

    public void setData(ArrayList<Stay> stay_list) {
        this.stay_list = stay_list;
        fireTableDataChanged();
    }
}
