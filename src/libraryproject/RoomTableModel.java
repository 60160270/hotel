/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Room;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author user
 */
public class RoomTableModel extends AbstractTableModel{
    ArrayList<Room> roomlist = new ArrayList<Room>();
    String[] columnRoom = {"Room ID", "Type", "Price", "Status"};
    @Override
    public int getRowCount() {
        return roomlist.size();
    }
    public String getColumnName(int column) {
        return columnRoom[column];
    }
    @Override
    public int getColumnCount() {
        return columnRoom.length;
        }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Room room = roomlist.get(rowIndex);
        switch (columnIndex){
            case 0 : return room.getRoom_id();
            case 1 : return room.getRoom_type();
            case 2 : return room.getRoom_price();
            case 3 : return room.getRoom_status();
            
        }
        return "";
    }
    public void setData(ArrayList <Room> roomlist){
        this.roomlist = roomlist;
        fireTableDataChanged();
    }
    
    
}
